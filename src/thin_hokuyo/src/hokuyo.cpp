#include "hokuyo.h"
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <termios.h>
#include <sstream>

#include <ros/console.h>

#if defined(__GNUC_GNU_INLINE__) || defined(__GNUC_STDC_INLINE__)
#define inline inline __attribute__ ((gnu_inline))
#endif

//parses an int of x bytes in the hokyo format
unsigned int parseInt(int bytes, char** s){
  int i;
  char* b=*s;
  unsigned int ret=0;
  int j=bytes-1;
  for (i=0; i<bytes;){
    if (*b==0||*b=='\n'){
      *s=0;
      return 0;
    }
    if (*(b+1)=='\n'){ //check for a wrapped line
      b++;
    } else {
      unsigned char c=*b-0x30;
      ret+=((unsigned int ) c)<<(6*j);
      i++;
      j--;
    }
    b++;
  }
  *s=b;
  return ret;
}


//skips a line
char* skipLine(char* buf){
  while (*buf!=0 && *buf!='\n')
    buf++;
  return (*buf=='\n')?buf+1:0;
}

//parses a reading response
void hokuyo_parseReading(HokuyoRangeReading* r, char* buffer, int remission)
{
  char* s=buffer;
  int expectedStatus=0;
  if (s[0]=='M')
    expectedStatus=99;
  if (s[0]=='C')
    expectedStatus=00;

  /*fprintf(stderr, "debug buffer %c%c\n", s[0], s[1]);*/
  
  int beamBytes=0;
  if (s[1]=='D' || s[1] == 'E') beamBytes=3;
  if (s[0]=='C') beamBytes=3;
  
  if (! beamBytes || ! expectedStatus){
    ROS_WARN("Invalid return packet, cannot parse reading");
    
    r->status=-1;
    return;
  }
  s+=2;
  char v[5];
  v[4]=0;
  strncpy(v,s,4); r->startStep=atoi(v); s+=4;
  strncpy(v,s,4); r->endStep=atoi(v);   s+=4;
  v[2]=0; strncpy(v,s,2); r->clusterCount=atoi(v);
  
  s=skipLine(s);
  if (s==0){
    ROS_ERROR( "error, line broken when reading the range parameters");
    r->status=-1;
    return;
  }

  strncpy(v,s,2); r->status=atoi(v); s+=2;

  if (r->status==expectedStatus){
  } else {
    ROS_ERROR_STREAM ("Error, Status=" << r->status);
    return;
  }
  r->timestamp=parseInt(4,&s);
  s=skipLine(s);

  int i=0;

  if (remission) {
    while(s!=0){
      r->ranges[i]=parseInt(beamBytes,&s);
      if (!s)
        break;
      r->remission[i]=parseInt(3,&s);
      ++i;
    }
    i--;
  } else {
    while(s!=0){
      r->ranges[i++]=parseInt(beamBytes,&s);
    }
    i--;
  }
  r->n_ranges=i;
}




unsigned int hokuyo_readPacket(HokuyoLaser* urg, char* buf, int bufsize, int faliures){
  int i;
  int failureCount=faliures;
  if (urg->fd<=0){
    ROS_ERROR_STREAM("Invalid " << urg->fd);
    return -1;
  }

  memset(buf, 0, bufsize);
  int wasLineFeed=0;
  char* b=buf;
  while (1){
    int c=read(urg->fd, b, bufsize);
    if (! c){
      //fprintf(stderr, "null" ); error or warning?
      ROS_ERROR("%s" , "null");
      usleep(25000);
      failureCount--;
    }else {
      for (i=0; i<c; i++){
	if (wasLineFeed && b[i]=='\n'){
	  b++;
	  return b-buf;
	}
	wasLineFeed=(b[i]=='\n');
      }
      b+=c;
    }
    if (failureCount<0)
      return 0;
  }
}

unsigned int hokuyo_readStatus(HokuyoLaser* urg, const char* cmd){
  char buf[HOKUYO_BUFSIZE];
  int tmp = write(urg->fd,  cmd, strlen(cmd)); (void)tmp;
  while (1){
    int c=hokuyo_readPacket(urg, buf, HOKUYO_BUFSIZE,10);
    if (c>0 && !strncmp(buf,cmd+1,strlen(cmd)-1)){
      char*s=buf;
      s=skipLine(s);
      char v[3]={s[0], s[1], 0};
      return atoi(v);
    }
  }
  return 0;
    
}


const std::string T = "dgs";
const char* HK_QUIT =  "\nQT\n";
const char* HK_SCIP = "\nSCIP2.0\n";
const char* HK_BEAM =  "\nBM\n";
const char* HK_RESET= "\nRS\n";
#define HK_SENSOR "\nPP\n"
#define wcm(cmd) write (urg->fd,  cmd, strlen(cmd))



int hokuyo_open_usb(HokuyoLaser* urg, const char* filename){   
  urg->isProtocol2=0;
  urg->isInitialized=0;
  urg->isContinuous=0;
  urg->fd=open(filename, O_RDWR| O_NOCTTY | O_SYNC); //| O_NONBLOCK);

  // set terminal communication parameters
  struct termios term;
  tcgetattr(urg->fd, &term);
  term.c_cflag = CS8 | CLOCAL | CREAD; // Character size mask 8 | Ignore modem control lines | Enable receiver
  term.c_iflag = IGNPAR; // Ignore framing errors and parity errors.
  term.c_oflag = 0;
  term.c_lflag = ICANON; // Enable canonical mode
  tcflush(urg->fd, TCIFLUSH);
  tcsetattr(urg->fd, TCSANOW, &term);
  usleep(200000);

  return urg->fd;
}

/*
int hokuyo_open_serial(HokuyoLaser* urg, const char* filename, int baudrate){
  fprintf(stderr, "Baudrate: %d\n", baudrate);
  if(carmen_serial_connect(&(urg->fd),filename)<0){
    fprintf(stderr,"error\n");
    fprintf(stderr,"  carmen_serial_connect failed!\n");
    return -1;
  }
  fprintf(stderr,"connected\n");
  carmen_serial_configure(urg->fd, 19200, "8N1");
  carmen_serial_set_low_latency(urg->fd);
  fprintf(stderr,"Serial Configured\n");
  carmen_serial_ClearInputBuffer(urg->fd);
  return 1;
}
*/

int hokuyo_init(HokuyoLaser* urg, int type)
{
  int i;
  int skipping = 0;

  if (type==0){ //urg
    urg->maxBeams=URG_MAX_BEAMS;
    urg->angularResolution=URG_ANGULAR_STEP;
    urg->maxRange = URG_MAX_RANGE;
  } else if (type==1){ //utm
    urg->maxBeams=UTM_MAX_BEAMS;
    urg->angularResolution=UTM_ANGULAR_STEP;
    urg->maxRange = UTM_MAX_RANGE;
  } else if (type==2){ //ubg
    ROS_INFO("Starting UBG");
    urg->maxBeams=UBG_MAX_BEAMS;
    urg->angularResolution=UBG_ANGULAR_STEP;
    urg->maxRange = UBG_MAX_RANGE;
  } else
    return -1;

  if (urg->fd<=0){
    return -1;
  }

  // stop the  device anyhow
  ROS_ERROR( "Stopping the device...");
  wcm(HK_QUIT);

  urg->isContinuous=0;
  
  // put the urg in SCIP2.0 Mode
  ROS_INFO("Switching to enhanced mode... ");
  int status=hokuyo_readStatus(urg, HK_SCIP);
  if (status==0){
    ROS_INFO("OK");
    urg->isProtocol2=1;
  } else {
    ROS_ERROR("Error. Unable to switch to SCIP2.0 Mode, please upgrade the firmware of your device");
    return -1;
  }

  
  ROS_INFO_STREAM("Printing device information"  << std::endl << "USE IT TO ADJUST HARDCODED VALUES!!!" );
  

  int wcnt = wcm(HK_SENSOR); (void) wcnt;
  char buf[HOKUYO_BUFSIZE];
  int c=hokuyo_readPacket(urg, buf, HOKUYO_BUFSIZE, 10);
  std::ostringstream oss;
  if (c > 0) {
    for (i = 0; i < c; ++i) {
      if (buf[i] == ';')
        skipping = 1;  
      else if (buf[i] == '\n')
        skipping = 0;
      if (! skipping)
        oss << (unsigned char)buf[i];
    }
    ROS_INFO("%s", oss.str().c_str());
  }
 
   ROS_INFO("Device initialized successfully");
  urg->isInitialized=1;
  return 1;
}

int hokuyo_startContinuous(HokuyoLaser* urg, int startStep, int endStep, int clusterCount, int remission){
  if (! urg->isInitialized)
    return -1;
  if (urg->isContinuous)
    return -1;

  // switch on the laser
  ROS_INFO("Switching on the laser emitter...  ");
  int status=hokuyo_readStatus(urg, HK_BEAM);
  if (! status){
    ROS_INFO("Ok");
  } else {
    ROS_INFO_STREAM("Error. Unable to control the laser, status is" <<status);
    return -1;
  }

  urg->startBeam = startStep;
  urg->endBeam   = endStep;
  urg->remission = remission;

  char command[1024];
  if (!remission) {
   sprintf (command, "\nMD%04d%04d%02d000\n", startStep, endStep, clusterCount);
    
  } else {
   sprintf (command, "\nME%04d%04d%02d000\n", startStep, endStep, clusterCount);
    
  }

  status=hokuyo_readStatus(urg, command);
  if (status==99 || status==0){
    ROS_INFO_STREAM("Continuous mode started with command" << command);
    urg->isContinuous=1;
    return 1;
  }
  ROS_ERROR_STREAM("Error. Unable to set the continuous mode, status=" << std::setw(2) << status);

  return -1;
}

int hokuyo_stopContinuous(HokuyoLaser* urg){
  if (! urg->isInitialized)
    return -1;
  if (! urg->isContinuous)
    return -1;

  int status=hokuyo_readStatus(urg, HK_QUIT);
  if (status==0){
    ROS_INFO( "OK");
    urg->isContinuous=0;
  } else {
    ROS_ERROR("Error. Unable to stop the laser");
    return -1;
  }
  return 1;
}

int hokuyo_reset(HokuyoLaser* urg){
  if (! urg->isInitialized)
    return -1;

  int status=hokuyo_readStatus(urg, HK_RESET);
  if (status==0){
    ROS_INFO("OK");
    urg->isContinuous=0;
  } else {
    ROS_ERROR("Error. Unable to reset laser");
    return -1;
  }
  return 1;
}

int hokuyo_close(HokuyoLaser* urg){
  if (! urg->isInitialized)
    return -1;
  hokuyo_stopContinuous(urg);
  close(urg->fd);
  urg->isProtocol2=0;
  urg->isInitialized=0;
  urg->isContinuous=0;
  urg->fd=-1;
  return 1;
}

