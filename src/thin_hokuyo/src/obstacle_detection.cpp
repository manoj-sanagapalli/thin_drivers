#include "ros/ros.h"
#include "sensor_msgs/LaserScan.h"
#include <geometry_msgs/Twist.h>
#include <sstream>
geometry_msgs::Twist command_velocity,cmd_vel;
static unsigned short test_range[1000];

void callback (const sensor_msgs::LaserScan::ConstPtr& msg)
{

    for(int i=0;i<70;i++) {
        test_range[i] = msg->ranges[i];
    }
} 

void cmd_callback (const geometry_msgs::Twist::ConstPtr& cmd)
{
cmd_vel.linear.x = cmd->linear.x;
cmd_vel.angular.z = cmd->angular.z;

}
int main(int argc, char **argv)
{
    std::string velocity,s;
    bool obstacle_detected;
    ros::init(argc,argv,"obstacle_detection");
    ros::NodeHandle n;
    n.param("velocity", velocity, std::string("/command_velocity"));
    ros::Subscriber sub = n.subscribe("scan",1000, callback);
    ros::Subscriber sub_keyop = n.subscribe("/keyop/cmd_vel",1000, cmd_callback);


    ros::Publisher pub_vel = n.advertise<geometry_msgs::Twist>(velocity, 1000);
    ros::Rate r(10);
    //command_velocity.linear.x=1.0;
    while (ros::ok())
    {
      ros::spinOnce();
      for (int j = 0; j < 70; j++)
        {
          if (test_range[j]<1.0)
           {
              n.setParam("obstacle_detected","detected");
              command_velocity.linear.x=0.0;
           } else {
             n.setParam("obstacle_detected","not detected");
              //command_velocity.linear.x=0.05;
           }
        }
      const char * det ="detected";
      n.getParam("obstacle_detected",s);
      if (!(strcmp(s.c_str(),det)))
      {
        //when obstacle is detected, velocity becomes zero
        pub_vel.publish(command_velocity);
      } else
      {
        //when there is no obstacle, the velocity comes from keyboard
        pub_vel.publish(cmd_vel);
      }
      r.sleep();
    }
    
    return 0;
}