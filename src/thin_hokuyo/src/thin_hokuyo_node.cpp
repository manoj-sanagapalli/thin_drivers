#include <ros/ros.h>
#include <string.h>
#include <sensor_msgs/LaserScan.h>
#include <geometry_msgs/Twist.h>
#include "hokuyo.h"
#include <iostream>

using namespace std;

static HokuyoLaser urg;
static sensor_msgs::LaserScan scan;
//geometry_msgs::Twist command_velocity;

char buf[HOKUYO_BUFSIZE];

int main(int argc, char** argv) {
  std::string topic, velocity, frame_id, serial_port, model, step_start, step_end, cluster_count, 
  remission, max_range , time_increment, scan_time, failures, maxInt_range, maxBeams;

  double _max_range, _time_increment, _scan_time;
  int  _step_start,_step_end, _cluster_count, _remission, _failures, _maxInt_range, _maxBeams;
  ros::init(argc, argv, "xtion",ros::init_options::AnonymousName);
  //ros::init(argc, argv, "xtion");
  ros::NodeHandle n("~");
  n.param("topic", topic, std::string("/scan"));
  //n.param("velocity", velocity, std::string("/command_velocity"));
  n.param("frame_id", frame_id, std::string("/laser_frame"));
  n.param("serial_port", serial_port, std::string("/dev/ttyACM2"));
  n.param("model", model, std::string("utm"));
  n.param("step_start", _step_start, 340);
  n.param("step_end", _step_end, 430);
  n.param("cluster_count", _cluster_count, 0);
  n.param("remission", _remission, 0);
  n.param("max_range", _max_range, 3.0);
  n.param("time_increment", _time_increment, 0.0);
  n.param("scan_time", _scan_time, 0.0);
  n.param("failures", _failures, 10);
  n.param("maxInt_range", _maxInt_range, 0);
  n.param("maxBeams", _maxBeams, 0);

  ROS_INFO("running with params: ");
  ROS_INFO_STREAM("_serial_port: " << serial_port);
  ROS_INFO_STREAM("_frame_id :" << frame_id);
  ROS_INFO_STREAM("_topic: " << topic);
  ROS_INFO_STREAM("_model: " << model);
  ROS_INFO_STREAM("step_start: " << _step_start);
  ROS_INFO_STREAM("step_end: " << _step_end);
  ROS_INFO_STREAM("_cluster_count: " << _cluster_count);
  ROS_INFO_STREAM("_remission: " << _remission);
  ROS_INFO_STREAM("_max_range: " << _max_range);
  ROS_INFO_STREAM("_time_increment: " << _time_increment);
  ROS_INFO_STREAM("_scan_time: " << _scan_time);
  ROS_INFO_STREAM("_failures: " << _failures);
  ROS_INFO_STREAM("_maxInt_range: " << _maxInt_range);
  ROS_INFO_STREAM("_maxBeams: " << _maxBeams);

  
  
  int max_int_range = _maxInt_range;
  int max_beams = _maxBeams;
    
  if (model == "urg") {
    max_int_range = URG_MAX_RANGE;
    max_beams = URG_MAX_BEAMS;
    scan.angle_increment = URG_ANGULAR_STEP;
  } else if (model == "ubg") {
    max_int_range = UBG_MAX_RANGE;
    max_beams = UBG_MAX_BEAMS;
    scan.angle_increment = UBG_ANGULAR_STEP;
  } else if (model == "utm") {
    max_int_range = UTM_MAX_RANGE;
    max_beams = UTM_MAX_BEAMS;
    scan.angle_increment = UTM_ANGULAR_STEP;
  } else {
    ROS_ERROR("unknown model, aborting" );
    return 0;
  }
  scan.ranges.resize(max_beams);
  scan.angle_min = -scan.angle_increment * max_beams/2;
  scan.angle_max = scan.angle_increment * max_beams/2;
  scan.range_min = 1e-3*max_int_range;
  // HACK
  scan.range_max = _max_range;
  scan.time_increment = _time_increment;
  scan.scan_time = _scan_time;

  ros::Publisher pub = n.advertise<sensor_msgs::LaserScan>(topic, 10);
  //ros::Publisher pub_vel = n.advertise<geometry_msgs::Twist>(velocity, 1000);
  

  int o=hokuyo_open_usb(&urg, serial_port.c_str());
  if (o<=0) {
    ROS_ERROR("failure in opening serial port") ;
    return -1;
  }

  o=hokuyo_init(&urg,1);
  if (o<=0){
    ROS_ERROR("failure in initializing device");
    return -1;
  }

  o=hokuyo_startContinuous(&urg, _step_start, _step_end, _cluster_count, _remission);
  if (o<=0){
    ROS_ERROR("failure in starting continuous mode");
    return -1;
  }

  ROS_INFO("device started");

  scan.header.seq = 0;
  scan.header.frame_id = frame_id;
  

  while (ros::ok()){
    hokuyo_readPacket(&urg, buf, HOKUYO_BUFSIZE,_failures);
    scan.header.stamp = ros::Time::now();
    HokuyoRangeReading reading;
    hokuyo_parseReading(&reading, buf, _remission);

    for (int i = 0; i < reading.n_ranges; i++) {
      if (reading.ranges[i] > 20 && reading.ranges[i] < 5601) {
        scan.ranges[i] = 1e-3 * reading.ranges[i];
      } else {
        scan.ranges[i] = scan.range_max;
      }

      /*if (scan.ranges[i] < 1.0) {
        command_velocity.linear.x = 0.0;
      } else {
        command_velocity.linear.x = 0.05;
      } */
    }

    
   
    pub.publish(scan);
    //pub_vel.publish(command_velocity);
  } // while run
  
  ROS_INFO("Closing");
  hokuyo_close(&urg);
  return 0;

  
}
