#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <signal.h>
#include <time.h>
#include <sys/time.h>
#include <sys/timeb.h>
#include <iostream>
#include "hokuyo.h"
#include <ros/ros.h>


static char output_file[80];
static FILE *fd=NULL;
static int run=1;

// return timestamp in ms from midnight
unsigned long getTimeStamp()
{
        struct timeval tv;
        gettimeofday(&tv,0);
        return (unsigned long)(tv.tv_sec%86400)*1000+(unsigned long)tv.tv_usec/1000;
}


void signalHandler(int)
{
    run = 0;
}


void logData(unsigned long ts, HokuyoRangeReading reading)
{
      if (!fd) {
        time_t now=time(NULL);
        struct tm *t = localtime(&now);
        sprintf(output_file,"%04d-%02d-%02d_%02d-%02d-%02d.log",
	        t->tm_year+1900, t->tm_mon+1, t->tm_mday, t->tm_hour, t->tm_min, t->tm_sec);
        ROS_INFO_STREAM("Log file: " << std::endl << output_file);
        fd = fopen(output_file, "w");
          if (! fd) {
	        perror("fopen");
	        return;
          }
      }

  ROS_INFO_STREAM(fd << ts);
  
  int i;
  for (i = 0; i < reading.n_ranges; ++i) {
    ROS_INFO_STREAM(fd << reading.ranges[i]);
  }
  ROS_INFO_STREAM(fd);
  
  fflush(fd);
}


int main (int argc, char** argv){
  ros::init(argc, argv, "test_node",ros::init_options::AnonymousName);
  ros::NodeHandle n_test("~");
  std::string step_start, cluster_count,failures,remission,valid_beams_count;
  int  i, _step_start, _cluster_count, _remission, _failures, _valid_beams_count;
  n_test.param("step_start", _step_start, 0);
  n_test.param("cluster_count", _cluster_count, 0);
  n_test.param("remission", _remission, 0);
  n_test.param("failures", _failures,10);
  n_test.param("valid_beams_count", _valid_beams_count,0);

  ROS_INFO_STREAM("_step_start: "<< _step_start);
  ROS_INFO_STREAM("_cluster_count: "<< _cluster_count);
  ROS_INFO_STREAM("_remission: "<< _remission);
  ROS_INFO_STREAM("_failures: "<< _failures);
  ROS_INFO_STREAM("_valid_beams_count: "<< _valid_beams_count);
  if (argc<2){
    ROS_INFO_STREAM("Usage: utm_test <device>");
    return 0;
  }
  
  signal(SIGABRT,&signalHandler);
  signal(SIGTERM,&signalHandler);
  signal(SIGINT,&signalHandler);

  char buf[HOKUYO_BUFSIZE];
  HokuyoLaser urg;
  int o=hokuyo_open_usb(&urg,argv[1]);
  if (o<=0)
    return -1;
  o=hokuyo_init(&urg,1);
  if (o<=0)
    return -1;
  o=hokuyo_startContinuous(&urg, _step_start, urg.maxBeams, _cluster_count, _remission);
  if (o<=0){
    return -1;
  }
  while (run){
    hokuyo_readPacket(&urg, buf, HOKUYO_BUFSIZE,_failures);
    // return timestamp in ms from midnight

    unsigned long ts = getTimeStamp();

    HokuyoRangeReading reading;
    hokuyo_parseReading(&reading, buf, _remission);

    //if we  get too much maxranges, restart the laser
    int validBeamsCount=_valid_beams_count;
    for (i=0; i<reading.n_ranges; i++){
      if (reading.ranges[i]>20 && reading.ranges[i]< 5601)
        validBeamsCount++;
    }

/*
    fprintf(stderr, "ts=%d, ss=%d, es=%d, cc=%d, nr=%d, st=%d vbc=%d\n", 
        reading.timestamp, 
        reading.startStep, 
        reading.endStep, 
        reading.clusterCount, 
        reading.n_ranges, 
        reading.status,
        validBeamsCount);
*/
    logData(ts,reading);
    
    /*
    printf("set size ratio -1\n");
    printf("plot [-5600:5600][-5600:5600]'-' w l\n");
    double initialAngle=-(0.5*urg.angularResolution*urg.maxBeams);
    for (i=0; i<reading.n_ranges; i++){
      double alpha=initialAngle*M_PI+urg.angularResolution*i;
      printf("%f %f\n", reading.ranges[i] * cos(alpha), reading.ranges[i] * sin (alpha)); 
    }
    printf ("e\n\n");
    */
    
  } // while run
  
  ROS_INFO("Closing.");
  hokuyo_close(&urg);
  return 0;
}
